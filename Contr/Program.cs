﻿Console.Clear();
Console.ForegroundColor=ConsoleColor.Green;
Console.WriteLine("Программа, которая выдаёт на выходе массив из строк, длиной менее или равным 3 символам:");
Console.ResetColor();

string[] arr=ArrayGenerator(10, min:1, max:10000);

Console.ForegroundColor=ConsoleColor.Yellow;
Console.WriteLine("Сгенерированный массив: ");
Console.ResetColor();

PrintArray(arr);

Console.ForegroundColor=ConsoleColor.Green;
Console.WriteLine();
Console.WriteLine("Массив из строк, длиной менее или равным 3 символам: ");
Console.ResetColor();

PrintArray(GetTrStringArray(arr));


string[] GetTrStringArray(string[] inArray)
{
    int k=0;
    string[] resultArray = new string[0];
    for (int i=0; i< inArray.Length; i++)
    {
        if (inArray[i].Length<=3)
        {
            k+=1;
            Array.Resize<string>(ref resultArray, k);
            resultArray[k-1]=inArray[i];
        }
    }
    return resultArray;

}


void PrintArray(string[] Array)
{

        for (int k = 0; k< Array.Length; k++)
        {

                Console.Write($" {Array[k]}");
          
        }
        Console.WriteLine();
}

string[] ArrayGenerator(int length,int min=0, int max=100)
{
    string[] array = new string[length];
    for (int i = 0; i < length; i++)
    {
        array[i] = (new Random().Next(min, max)).ToString();
    }
    return array;
}
