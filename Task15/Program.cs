﻿IsWeekend(InputWeekDay());

int InputWeekDay()
{

    int inputNumber;
    while (true)
    {
        try
        {
            Console.WriteLine("Введите номер дня недели (1-7): ");
            inputNumber = int.Parse(Console.ReadLine());
            if (inputNumber >= 1 && inputNumber <= 7)
            {
                break;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такого дня недели нет: ");
                Console.ResetColor();
            }
        }
        catch
        {
            Console.WriteLine("Вы ввели не число!");
        }
    }
    return inputNumber;
}

void IsWeekend(int digit)
{
    if (digit == 6 || digit == 7)
    {
        Console.WriteLine("Да, выходной");
    }
    else
    {
        Console.WriteLine("Нет, не выходной");
    }
}