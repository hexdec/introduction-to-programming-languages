﻿Console.Clear();
Console.WriteLine("Программа вычисления суммы цифр в числе");
GetDigitSum(InputDigit("Введите число: "));

int InputDigit(string message = "")
{

    int inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                MenuTextOut(yellow:message,newline:false);
            }
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

void GetDigitSum(int number)
{
    int sum = 0;
    string str = number.ToString();
    for (int i = 0; i < str.Length; i++)
    {
        sum += int.Parse(str[i].ToString());
    }
    MenuTextOut(green: $"Сумма цифр: ", white: $"{sum}");
}

void MenuTextOut(string white = "", string green = "", string yellow = "", bool newline = true)
{
    if (green != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.Write(green);
        Console.ResetColor();
    }
    if (yellow != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(yellow);
        Console.ResetColor();
    }
    if (newline)
    {
        Console.WriteLine(white);
    }
    else
    {
        Console.Write(white);
    }

}