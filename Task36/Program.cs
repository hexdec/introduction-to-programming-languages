﻿Console.Clear();
int maxLength=10;
int arrayMinDigit=-100;
int arrayMaxDigit=100;
int arrayLength= new Random().Next(1,maxLength);

int[] array=ArrayGenerator(arrayLength,min:arrayMinDigit,max:arrayMaxDigit);

MenuTextOut(yellow:"Сгенерирован массив длинной ",newline:false);
MenuTextOut(green:$"{arrayLength}");
foreach (int i in array[0..(array.Length)])
{
    MenuTextOut($"{i} ",newline:false);
}
Console.WriteLine();
MenuTextOut(yellow:"Сумма элементов, стоящих на нечётных позициях: ",newline:false);
MenuTextOut(green:$"{GetOddSum(array)}");

int GetOddSum(int[] inArray)
{
    int sum=0;
    int counter=0;
    foreach(int i in inArray[0..(inArray.Length)])
    {
        if (counter%2>0) sum+=i;
        counter++;
    }
    return sum;
}


int[] ArrayGenerator(int length,int min=0, int max=100)
{
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
        array[i] = new Random().Next(min, max);
    }
    return array;
}

void MenuTextOut(string white = "", string green = "", string yellow = "", bool newline = true)
{
    if (green != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.Write(green);
        Console.ResetColor();
    }
    if (yellow != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(yellow);
        Console.ResetColor();
    }
    if (newline)
    {
        Console.WriteLine(white);
    }
    else
    {
        Console.Write(white);
    }

}