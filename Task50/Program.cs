﻿Console.Clear();
int[,] biArray = BiArrayGenerator(new Random().Next(2,7),new Random().Next(2,7));
PrintBiArray(biArray);
FindElement(biArray);

void FindElement(int[,] inBiArray)
{
    int length1=inBiArray.GetLength(0);
    int length2=inBiArray.GetLength(1);

    int indexCorrector=1; /* "0" for index enumerate */

/*
    Console.Write("Введите номер элемента: ");
    string input=Console.ReadLine();
    if (input=="q") return;
*/
    int searchableDigit=InputDigit(message: "Введите номер элемента: ");

    if ( searchableDigit <= inBiArray.Length && searchableDigit > 0 ) 
    {
        int result = inBiArray[(searchableDigit-indexCorrector) / length2, (searchableDigit-indexCorrector) % length2];
        Console.WriteLine($"Элемент массива c номером {searchableDigit}: {result}");
    }
    else
    {
        Console.WriteLine("такого элемента в массиве нет");
    }
    
    FindElement(inBiArray);
    

}

void PrintBiArray(int[,] biArray)
{   Console.ForegroundColor=ConsoleColor.Yellow;
    Console.WriteLine("Сгенерированный массив: ");
    Console.ResetColor();
    for (int i = 0; i < biArray.GetLength(0); i++)
    {

        for (int k = 0; k< biArray.GetLength(1); k++)
        {
            if (biArray[i,k] >= 0) 
            {
                Console.Write($" {biArray[i,k]} ");
            }
            else
            {
                Console.Write($"{biArray[i,k]} ");
            }
            
        }
    Console.WriteLine();        
    }
}

int[,] BiArrayGenerator(int length1=2,int length2=2)
{
    int[,] array = new int[length1, length2];
    for (int i = 0; i < length1; i++)
    {
        for (int k = 0; k< length2; k++)
        {
            array[i,k] = new Random().Next(-100, 100);
        }
        
    }
    return array;
}

int InputDigit(string message = "")
{

    int inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}