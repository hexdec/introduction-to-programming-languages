﻿int N;
while (true) {
    try {
        Console.WriteLine("Введите число: ");
        N = int.Parse(Console.ReadLine());
        break;
    } catch {
        Console.WriteLine("Вы ввели не число!");
        continue;
    }
}
for (int i=2;i<=N;i=i+2) {
    Console.Write(i+" ");
}