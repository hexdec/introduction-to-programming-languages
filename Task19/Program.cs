﻿if (IsPalindrome(InputDigit()))
{
    Console.WriteLine("Да");
}
else
{
    Console.WriteLine("Нет");
}


int InputDigit()
{

    int inputNumber;
    while (true)
    {
        try
        {
            Console.WriteLine("Введите число: ");
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

bool IsPalindrome(int number)
{
    string tempStr = number.ToString();
    bool isTrue = true;
    for (int i = 0; i < (tempStr.Length / 2); i++)
    {
        if (tempStr[i] != tempStr[tempStr.Length - (i + 1)])
        {
            isTrue = false;
            break;
        }
    }
    return isTrue;
}
