﻿Console.Clear();
int maxLength=10;
int arrayLength= new Random().Next(1, maxLength);

int[] array=ArrayGenerator(arrayLength);

MenuTextOut(yellow:"Сгенерирован массив длинной ",newline:false);
MenuTextOut(green:$"{arrayLength}");
foreach (int i in array[0..(array.Length)])
{
    MenuTextOut($"{i} ",newline:false);
}
Console.WriteLine();
MenuTextOut(yellow:"Количество чётных элементов: ",newline:false);
MenuTextOut(green:$"{GetEvenCount(array)}");

int GetEvenCount(int[] inArray)
{
    int count=0;
    foreach(int i in inArray[0..(inArray.Length)])
    {
        if (i%2==0) count++;
    }
    return count;
}


int[] ArrayGenerator(int length)
{
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
        array[i] = new Random().Next(100, 1000);
    }
    return array;
}

void MenuTextOut(string white = "", string green = "", string yellow = "", bool newline = true)
{
    if (green != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.Write(green);
        Console.ResetColor();
    }
    if (yellow != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(yellow);
        Console.ResetColor();
    }
    if (newline)
    {
        Console.WriteLine(white);
    }
    else
    {
        Console.Write(white);
    }

}