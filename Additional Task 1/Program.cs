﻿int inputVar = InputNumber();
for (int i = 1; i <= inputVar; i++)
{
    if (IsSimple(i))
    {
        Console.Write(i + " ");
    }
}


int InputNumber()
{
    int inputNumber;
    while (true)
    {
        try
        {
            Console.WriteLine("Введите число: ");
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.WriteLine("Вы ввели не число!");
        }
    }
    return inputNumber;
}

bool IsSimple(int digit)
{
    bool result = true;
    int z = (digit % 2) + (digit / 2);
    for (int i = 2; i <= z; i++)
    {
        if (digit % i == 0)
        {
            result = false;
            break;
        }
    }
    return result;
}