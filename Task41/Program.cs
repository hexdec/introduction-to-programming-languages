﻿Console.Clear();
Console.Write("Введите количество чисел: ");
int length=int.Parse(Console.ReadLine());

Console.WriteLine($"Количество чисел больше 0:  {GreaterDigit(length)}");



int GreaterDigit(int counter, int resultCount=0)
{
    if (counter <= 0)
    {
        return resultCount;
    } 
    int inputDigit= InputDigit(message:"Введите целое число: ");
    if (inputDigit > 0) resultCount++;

    return GreaterDigit(--counter,resultCount);

}


int InputDigit(string message = "")
{

    int inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}