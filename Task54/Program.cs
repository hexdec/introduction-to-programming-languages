﻿Console.Clear();
Console.ForegroundColor=ConsoleColor.Green;
Console.WriteLine("Программа, которая упорядочит по убыванию элементы каждой строки двумерного массива");
Console.ResetColor();
Console.ForegroundColor=ConsoleColor.Yellow;
Console.WriteLine("Сгенерированный массив: ");
Console.ResetColor();
int[,] biArray = BiArrayGenerator(new Random().Next(2,7),new Random().Next(2,7));
PrintBiArray(biArray);


Console.ForegroundColor=ConsoleColor.Yellow;
Console.WriteLine("Отсортированный массив: ");
Console.ResetColor();
PrintBiArray(SortBiArray(biArray));

int[,] SortBiArray(int[,] inBiArray)
{
    int length1=inBiArray.GetLength(0);
    int length2=inBiArray.GetLength(1);

    for (int i=0; i < length1;i++)
    {
        for (int k=0; k < length2; k++)
        {
            int index=k;
            int max=inBiArray[i,k];
            for (int p=k; p < length2; p++)
            {

                if (inBiArray[i,p]>max)
                {
                    
                    max=inBiArray[i,p];
                    index=p;
                }
            }
            int temp=inBiArray[i,index];
            inBiArray[i,index]=inBiArray[i,k];
            inBiArray[i,k]=temp;
        }
    }
    return inBiArray;
}

void PrintBiArray(int[,] biArray)
{   
    for (int i = 0; i < biArray.GetLength(0); i++)
    {

        for (int k = 0; k< biArray.GetLength(1); k++)
        {
            if (biArray[i,k] >= 0) 
            {
                Console.Write($" {biArray[i,k]} ");
            }
            else
            {
                Console.Write($"{biArray[i,k]} ");
            }
            
        }
    Console.WriteLine();        
    }
    for (int i=0; i < biArray.GetLength(1); i++)   Console.Write("---");
    Console.WriteLine();
}

int[,] BiArrayGenerator(int length1=2,int length2=2)
{
    int[,] array = new int[length1, length2];
    for (int i = 0; i < length1; i++)
    {
        for (int k = 0; k< length2; k++)
        {
            array[i,k] = new Random().Next(0, 10);
        }
        
    }
    return array;
}