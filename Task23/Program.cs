﻿
GetCube(InputDigit());

int InputDigit()
{

    int inputNumber;
    while (true)
    {
        try
        {
            Console.WriteLine("Введите число: ");
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}


void GetCube(int count, int power = 3)
{
    Console.ForegroundColor=ConsoleColor.Green;
    Console.Write($"Кубы чисел от 1 до {count}: ");
    Console.ResetColor();
    Console.Write("1");
    for (int i = 2; i <= count; i++)
    {
        Console.Write(",");
        Console.Write($" {Math.Pow(i, power)}");
    }
}