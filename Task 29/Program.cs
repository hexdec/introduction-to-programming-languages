﻿
Console.ForegroundColor = ConsoleColor.Yellow;
Console.Write("Сгенерированный массив: ");
Console.ResetColor();
ArrayPrint(ArrayGenerator(8));

void ArrayPrint(int[] inArray)
{
    Console.Write("[");
    foreach (int i in inArray[0..(inArray.Length - 1)])
    {
        Console.Write("{0}, ", i);
    }
    Console.Write(inArray.Last());
    Console.Write("]");
}

int[] ArrayGenerator(int length)
{
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
        array[i] = new Random().Next(-100, 100);
    }
    return array;
}