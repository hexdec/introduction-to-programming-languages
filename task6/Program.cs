﻿int a;
while (true) {
    try {
        Console.WriteLine("Введите число: ");
        a = int.Parse(Console.ReadLine());
        break;
    } catch {
        Console.WriteLine("Вы ввели не число!");
        continue;
    }
}

if (a % 2 == 0) {
    Console.WriteLine("Число "+a+" чётное.");
} else {
    Console.WriteLine("Число "+a+" нечётное.");
}