﻿Console.Clear();
Console.ForegroundColor=ConsoleColor.Green;
Console.WriteLine("Генератор двумерного массива m×n");
Console.ResetColor();
int m=InputDigit("Введите m: ");
int n=InputDigit("Введите n: ");

PrintBiArray(BiArrayGenerator(m,n));


void PrintBiArray(int[,] biArray)
{
    for (int i = 0; i < biArray.GetLength(0); i++)
    {
        Console.WriteLine();
        for (int k = 0; k< biArray.GetLength(1); k++)
        {
            if (biArray[i,k] >= 0) 
            {
                Console.Write($" {biArray[i,k]} ");
            }
            else
            {
                Console.Write($"{biArray[i,k]} ");
            }
            
        }
        
    }
}



int[,] BiArrayGenerator(int length1,int length2)
{
    int[,] array = new int[length1, length2];
    for (int i = 0; i < length1; i++)
    {
        for (int k = 0; k< length2; k++)
        {
            array[i,k] = new Random().Next(-100, 100);
        }
        
    }
    return array;
}

int InputDigit(string message = "")
{

    int inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}