﻿int inputNumber;
while (true)
{
    try
    {
        Console.WriteLine("Введите трёхзначное число : ");
        inputNumber = int.Parse(Console.ReadLine());
        if ((inputNumber < 1000) && (inputNumber >= 100))
        {
            break;
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не трёхзначное число: ");
            Console.ResetColor();
        }
    }
    catch
    {
        Console.WriteLine("Вы ввели не число!");
    }
}
Console.ForegroundColor = ConsoleColor.Green;
Console.Write("Вторая цифра: ");
Console.ResetColor();
Console.WriteLine(GetSecondDigit(inputNumber));


int GetSecondDigit(int digit)
{
    return ((digit % 100) / 10 );
}