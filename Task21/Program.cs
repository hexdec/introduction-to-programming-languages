﻿int[] pointA = InputDigit("A");
int[] pointB = InputDigit("B");

Console.WriteLine("Расстояние между точками:" + PointsDistance(pointA, pointB));

int[] InputDigit(string pointName)
{

    string inputString;
    int[] inputNumber = new int[3];
    while (true)
    {
        try
        {
            Console.WriteLine($"Введите координаты точки {pointName} через запятую x, y, z: ");
            inputString = Console.ReadLine();
            //for (int i=0;i<3;i++)
            //{
            //    inputNumber[i] = int.Parse(inputString.Split(",")[i])
            //}
            int[] TempNumber = { int.Parse(inputString.Split(",")[0]), int.Parse(inputString.Split(",")[1]), int.Parse(inputString.Split(",")[2]) };
            inputNumber = TempNumber;
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

double PointsDistance(int[] point1, int[] point2)
{
    double qsum = 0;
    for (int i = 0; i < 3; i++)
    {
        qsum += Math.Pow((point1[i] - point2[i]), 2);
    }
    double distance = Math.Sqrt(qsum);
    return distance;
}