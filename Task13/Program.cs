﻿int input=InputDigit();
Console.ForegroundColor = ConsoleColor.Green;
Console.Write("Третья цифра: ");
Console.ResetColor();
Console.WriteLine(GetSecondDigit(input));


int InputDigit()
{

    int inputNumber;
    while (true)
    {
        try
        {
            Console.WriteLine("Введите минимум трёхзначное число: ");
            inputNumber = int.Parse(Console.ReadLine());
            if (inputNumber >= 100)
            {
                break;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Третьей цифры нет: ");
                Console.ResetColor();
            }
        }
        catch
        {
            Console.WriteLine("Вы ввели не число!");
        }
    }
    return inputNumber;
}

int GetSecondDigit(int digit)
{
    while (digit >= 1000)
    {
        digit=digit / 10;
    }
    return ((digit % 100) % 10);

}