﻿Console.Clear();
Console.WriteLine("Программа возведения числа A в натуральную степень B");
Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Ответ: " + GetPower(InputFractDigit(message: "Введите число A: "), InputDigit(message: "Введите число B: ")));
Console.ResetColor();

double InputFractDigit(string message = "")
{

    double inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = double.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

int InputDigit(string message = "")
{

    int inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}


double GetPower(double digit, int pow)
{
    if (pow == 0) return 1;
    double result = digit;
    for (int i = 1; i < pow; i++)
    {
        result *= digit;
    }
    return result;
}