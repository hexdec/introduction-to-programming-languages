﻿Console.WriteLine("Заданы 2 прямые уравнениями  y = k1 * x + b1, y = k2 * x + b2");
double k1,b1,k2,b2;
k1=InputFractDigit("Введите переменную k1: ");
b1=InputFractDigit("Введите переменную b1: ");
k2=InputFractDigit("Введите переменную k2: ");
b2=InputFractDigit("Введите переменную b2: ");


double[,] ip=GetIntersectionPoint(k1:k1,k2:k2,b1:b1,b2:b2);

Console.WriteLine($"Точка пересечения: ({ip[0,0]},{ip[0,1]})");


double[,] GetIntersectionPoint (double k1,double b1,double k2,double b2)
{
    double ipX=(b2-b1)/(k1-k2);
    double ipY=k1*ipX+b1;

    return  new double[,] {{ipX,ipY}};
}

double InputFractDigit(string message = "")
{

    double inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = double.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}