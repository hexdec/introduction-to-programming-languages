﻿Menu();

void Menu()
{
    Console.Clear();
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Выберите фигуру: ");
    Console.ResetColor();
    string[] param0 = {
        "1. Круг",
        "2. Треугольник",
        "3. Прямоугольник"
    };
    Array.ForEach(param0, Console.WriteLine);
    int selector = SelectorRanger(param0);

    switch (selector)
    {
        case 1:
            MenuCircle();
            break;
        case 2:
            MenuTriangle();
            break;
        case 3:
            MenuRectangle();
            break;
    }
}

void MenuCircle()
{
    Console.Clear();
    string[] paramsMenuCircle =
    {
        "1. Радиус",
        "2. Диаметр",
        "3. Длина окружности"
    };
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Выберите параметры для ввода: ");
    Console.ResetColor();
    Array.ForEach(paramsMenuCircle, Console.WriteLine);
    int selector = SelectorRanger(paramsMenuCircle);
    Console.Clear();
    switch (selector)
    {
        case 1:
            Console.WriteLine("Введите радиус окружности:");
            SquareCircle(r: InputFractDigit("Радиус окружности: "));
            break;
        case 2:
            Console.WriteLine("Введите диаметр окружности:");
            SquareCircle(d: InputFractDigit("Диаметр окружности: "));
            break;
        case 3:
            Console.WriteLine("Введите длину окружности:");
            SquareCircle(l: InputFractDigit("Длина окружности:"));
            break;
    }
}

void MenuTriangle()
{
    string[] paramsMenu =
    {
        "1. Длина сторон",
        "2. Основание и высота",
        "3. Длинна сторон a, b и угол между ними"
    };
    Console.Clear();
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Выберите параметры для ввода: ");
    Console.ResetColor();
    Array.ForEach(paramsMenu, Console.WriteLine);
    int selector = SelectorRanger(paramsMenu);
    Console.Clear();
    switch (selector)
    {
        case 1:
            Console.WriteLine("Введите длину сторон треугольника a b c:");
            SquareTriangle(a: InputFractDigit("Сторона треугольника a: "), b: InputFractDigit("Сторона треугольника b: "), c: InputFractDigit("Сторона треугольника c: "));
            break;
        case 2:
            Console.WriteLine("Введите длину основания треугольника a и высоты h:");
            SquareTriangle(a: InputFractDigit("Сторона треугольника a: "), h: InputFractDigit("Высота треугольника h: "));
            break;
        case 3:
            Console.WriteLine("Введите длину двух сторон треугольника a,b и угол между ними в градусах:");
            SquareTriangle(a: InputFractDigit("Сторона треугольника a: "), b: InputFractDigit("Сторона треугольника b: "), sin: InputFractDigit("угол (в градусах): "));
            break;
    }
}

void MenuRectangle()
{
    string[] paramsMenu =
    {
        "1. Длина сторон a, b",
        "2. Сторона и диагональ"
    };
    Console.Clear();
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Выберите параметры для ввода: ");
    Console.ResetColor();
    Array.ForEach(paramsMenu, Console.WriteLine);
    int selector = SelectorRanger(paramsMenu);
    Console.Clear();
    switch (selector)
    {
        case 1:
            Console.WriteLine("Введите длину сторон прямоугольника a b:");
            SquareRectangle(a: InputFractDigit("Сторона прямоугольника a: "), b: InputFractDigit("Сторона прямоугольника b: "));
            break;
        case 2:
            Console.WriteLine("Введите длину стороны прямоугольника a и длину диагонали AB:");
            SquareRectangle(a: InputFractDigit("Сторона прямоугольника a: "), diag: InputFractDigit("Длина диагонали AB: "));
            break;
    }
}

int SelectorRanger(string[] parameters)
{
    int selector = 0;
    while (selector > parameters.Length || selector < 1)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("Введите номер пункта меню: ");
        Console.ResetColor();
        selector = InputDigit();
    }
    return selector;
}

int InputDigit()
{

    int inputNumber;
    while (true)
    {
        try
        {
            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Введите номер пункта меню: ");
            //Console.ResetColor();
            inputNumber = int.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

double InputFractDigit(string message = "")
{

    double inputNumber;
    while (true)
    {
        try
        {
            if (message != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(message);
                Console.ResetColor();
            }
            inputNumber = double.Parse(Console.ReadLine());
            break;
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вы ввели не число!");
            Console.ResetColor();
        }
    }
    return inputNumber;
}

void SquareTriangle(double a = 0, double b = 0, double c = 0, double h = 0, double sin = 0)
{
    //
    double square = 0;
    if (a != 0 && b != 0 && c != 0)
    {
        double p = (a + b + c) / 2;
        square = Math.Sqrt(p * (p - a) * (p - b) * (p - c));
    }
    if (a != 0 && h != 0)
    {
        square = 0.5 * a * h;
    }
    if (a != 0 && b != 0 && sin != 0)
    {
        double rad = (sin * (Math.PI)) / 180;
        square = 0.5 * a * b * Math.Sin(rad);
    }
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine($"Площадь треугольника: {square}");
    Console.ResetColor();
}

void SquareRectangle(double a, double b = 0, double diag = 0)
{
    double square = 0;
    if (b != 0 && a != 0)
    {
        square = a * b;
    }
    if ((diag != 0) && (a != 0) && (diag > a))
    {
        square = a * Math.Sqrt(diag * diag - a * a);
    }
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine($"Площадь прямоугольника: {square}");
    Console.ResetColor();
}

void SquareCircle(double r = 0, double d = 0, double l = 0)
{
    double square;
    if (r == 0)
    {
        if (d != 0)
        {
            r = d / 2;
        }
        if (l != 0)
        {
            r = l / (2 * Math.PI);
        }
    }
    square = Math.PI * Math.Pow(r, 2);
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine($"Площадь окружности: {square}");
    Console.ResetColor();
}