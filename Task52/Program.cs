﻿Console.Clear();
Console.ForegroundColor=ConsoleColor.Green;
Console.WriteLine("Программа вычисления среднего арифметического элементов в каждом столбце");
Console.ResetColor();
int[,] biArray = BiArrayGenerator(new Random().Next(2,7),new Random().Next(2,7));
PrintBiArray(biArray);
GetBiArrayArithMean(biArray);

void GetBiArrayArithMean(int[,] inBiArray)
{
    int length1=inBiArray.GetLength(0);
    int length2=inBiArray.GetLength(1);

    double[] result = new double[length2];
    for (int k=0; k < length2;k++)
    {
        int sum = 0;
        for (int i = 0; i < length1;i++)
        {
            sum += inBiArray[i,k];
        }
        result[k]=Math.Round(Convert.ToDouble(sum)/Convert.ToDouble(length1),2);
    }

    foreach (double i in result)
    {
        Console.Write(i+"; ");
    }

}

void PrintBiArray(int[,] biArray)
{   Console.ForegroundColor=ConsoleColor.Yellow;
    Console.WriteLine("Сгенерированный массив: ");
    Console.ResetColor();
    for (int i = 0; i < biArray.GetLength(0); i++)
    {

        for (int k = 0; k< biArray.GetLength(1); k++)
        {
            if (biArray[i,k] >= 0) 
            {
                Console.Write($" {biArray[i,k]} ");
            }
            else
            {
                Console.Write($"{biArray[i,k]} ");
            }
            
        }
    Console.WriteLine();        
    }
    for (int i=0; i < biArray.GetLength(1); i++)   Console.Write("---");
    Console.WriteLine();
}

int[,] BiArrayGenerator(int length1=2,int length2=2)
{
    int[,] array = new int[length1, length2];
    for (int i = 0; i < length1; i++)
    {
        for (int k = 0; k< length2; k++)
        {
            array[i,k] = new Random().Next(0, 10);
        }
        
    }
    return array;
}