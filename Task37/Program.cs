﻿Console.Clear();
int maxLength=10;
int arrayMinDigit=1;
int arrayMaxDigit=10;
int arrayLength= new Random().Next(1,maxLength);

int[] array=ArrayGenerator(arrayLength,min:arrayMinDigit,max:arrayMaxDigit);

MenuTextOut(yellow:"Сгенерирован массив длинной ",newline:false);
MenuTextOut(green:$"{arrayLength}",yellow:": ");
foreach (int i in array[0..(array.Length)])
{
    MenuTextOut($"{i} ",newline:false);
}
Console.WriteLine();
 MenuTextOut(yellow:"Произведение пар чисел в одномерном массиве: ");

int[] resultArray=GetMultiPair(array);
foreach (int i in resultArray[0..(resultArray.Length)])
{
    MenuTextOut($"{i} ",newline:false);
}




int[] GetMultiPair(int[] inArray)
{
    int resultLength=inArray.Length/2+inArray.Length%2;
    int[] resultArray= new int[resultLength];
    for (int i = 0; i < resultLength; i++)
    {
        if ((inArray.Length-1-i) != i)
        {
            resultArray[i]=inArray[i]*inArray[inArray.Length-1-i];
        } else
        {
            resultArray[i]=inArray[i];
        }
        
    }

    return resultArray;
}


int[] ArrayGenerator(int length,int min=0, int max=100)
{
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
        array[i] = new Random().Next(min, max);
    }
    return array;
}

void MenuTextOut(string white = "", string green = "", string yellow = "", bool newline = true)
{
    if (green != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.Write(green);
        Console.ResetColor();
    }
    if (yellow != string.Empty)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(yellow);
        Console.ResetColor();
    }
    if (newline)
    {
        Console.WriteLine(white);
    }
    else
    {
        Console.Write(white);
    }

}